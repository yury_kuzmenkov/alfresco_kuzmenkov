package com.temp.evaluator;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class CheckIfNodeHasCountableAspectEvaluator extends BaseEvaluator {

    private static final String ASPECT_COUNTABLE = "temp:countable";

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        try {
            JSONArray nodeAspects = getNodeAspects(jsonObject);
            if (nodeAspects == null) {
                return false;
            } else {
                if (nodeAspects.contains(ASPECT_COUNTABLE)) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception err) {
            throw new RuntimeException("JSONException whilst running action evaluator: " + err.getMessage());
        }
    }
}
