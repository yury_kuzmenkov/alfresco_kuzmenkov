<import resource="classpath:/alfresco/templates/org/alfresco/import/alfresco-util.js">

function main()
{
    AlfrescoUtil.param("nodeRef");
    AlfrescoUtil.param("site", null);
    AlfrescoUtil.param("libraryRoot", null);
    var nodeDetails = AlfrescoUtil.getNodeDetails(model.nodeRef, model.site, null, model.libraryRoot);
    model.node = nodeDetails.item.node;
}

main();