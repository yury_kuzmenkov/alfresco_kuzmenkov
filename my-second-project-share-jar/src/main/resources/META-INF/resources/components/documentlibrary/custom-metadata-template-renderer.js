(function () {
    YAHOO.Bubbling.fire("registerRenderer",
        {
            propertyName: "tempDocumentIdCustomRendition",
            renderer: function tempCount_renderer(record, label) {
                   var jsNode = record.jsNode,
                    properties = jsNode.properties,
                    html = "";
                   tempCount = properties["temp:count"] || "";
                html = '<span>' + label + '<h2>' + tempCount + '</h2></span>';
                return html;
            }
        });
/*
    YAHOO.Bubbling.fire("registerAction",
        {
            actionName: "onActionCallWebScript",
            fn: function org_alfresco_training_onShowCustomMessage(file) {
                Alfresco.util.PopupManager.displayMessage(
                    {
                        text: this.msg("alfresco.tutorials.doclib.action.showCustomMessage.text",
                            file.displayName, Alfresco.constants.USERNAME)
                    });
            }
        }); */
})();