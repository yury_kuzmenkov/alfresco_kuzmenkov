package com.temp.actions.customactions;

import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class IncrementPropertyActionExecuter extends ActionExecuterAbstractBase {

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {

    }

    @Override
    protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
        if (serviceRegistry.getNodeService().exists(actionedUponNodeRef)){
            NodeService nodeService = serviceRegistry.getNodeService();
            if (nodeService.exists(actionedUponNodeRef)){
                NamespaceService namespaceService = serviceRegistry.getNamespaceService();
                QName aspectCountable = QName.createQName("temp:countable", namespaceService);
                QName aspectPropertyCount = QName.createQName("temp:count", namespaceService);
                Map<QName, Serializable> properties = nodeService.getProperties(actionedUponNodeRef);
                Integer aspectPropertyCountValue = 0;
                if (nodeService.hasAspect(actionedUponNodeRef,aspectCountable)){
                    Serializable property = nodeService.getProperty(actionedUponNodeRef, aspectPropertyCount);
                    aspectPropertyCountValue = Integer.valueOf((Integer) property);
                    aspectPropertyCountValue++;
                    property = (Serializable) aspectPropertyCountValue;
                    properties.put(aspectPropertyCount, property);
                    nodeService.setProperties(actionedUponNodeRef, properties);
                }else {
                    properties.put(aspectPropertyCount, aspectPropertyCountValue++);
                    nodeService.addAspect(actionedUponNodeRef, aspectCountable, properties);
                }
            }
        }
    }



//    @Override
//    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
//        paramList.add(
//                new ParameterDefinitionImpl(               // Create a new parameter definition to add to the list
//                        INCREMENT_ASPECT_PROPERTY,                           // The name used to identify the parameter
//                        DataTypeDefinition.INT,         // The parameter value type
//                        false,               // Indicates whether the parameter is mandatory
//                        getParamDisplayLabel(INCREMENT_ASPECT_PROPERTY)));   // The parameters display label
//    }
}
