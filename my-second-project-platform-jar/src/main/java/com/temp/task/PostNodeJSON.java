package com.temp.task;

import com.temp.exceptionRespons.ExceptionConstants;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PostNodeJSON extends AbstractWebScript {

    static final Logger logger = LogManager.getLogger(PostNodeJSON.class);

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {

        JSONObject jsonContent = (JSONObject) webScriptRequest.parseContent();
        if (jsonContent == null){
            logger.error(ExceptionConstants.EXCEPTION_EMPTY_CONTENT);
            throw new WebScriptException(Status.STATUS_BAD_REQUEST , ExceptionConstants.EXCEPTION_EMPTY_CONTENT);
        }
        try {
            String parentNodeRefStr = jsonContent.getString("parentNodeRef");
            String nodeName = jsonContent.getString("nodeName");
            JSONObject typeObject = jsonContent.getJSONObject("type");
            String typeName = typeObject.getString("name");
            JSONArray typeProperties = typeObject.getJSONArray("properties");
            JSONArray aspectsArray = jsonContent.getJSONArray("aspects");

            if (NodeRef.isNodeRef(parentNodeRefStr)) {
                NodeRef parentNodeRef = new NodeRef(parentNodeRefStr);
                NodeService nodeService = serviceRegistry.getNodeService();
                NamespaceService namespaceService = serviceRegistry.getNamespaceService();

                if (nodeService.exists(parentNodeRef)) {
                    QName containsAssocType = ContentModel.ASSOC_CONTAINS;//ContentModel.ASSOC_CHILDREN;
                    QName assocName = QName.createQName(containsAssocType.getNamespaceURI(), nodeName);
                    QName nodeType = QName.createQName(typeName, namespaceService);


                    Map<QName, Serializable> typePropertiesMap = new HashMap<>();
                    for (int i = 0; i < typeProperties.length(); i++) {
                        JSONObject property = typeProperties.getJSONObject(i);
                        QName propertyName = QName.createQName(property.getString("name"), namespaceService);
                        Serializable propertyValue = (Serializable) property.get("value");
                        typePropertiesMap.put(propertyName, propertyValue);
                    }
                    typePropertiesMap.put(QName.createQName("cm:name", namespaceService), nodeName);


                    NodeRef childNodeRef = nodeService.getChildByName(parentNodeRef, containsAssocType, nodeName);

                    if (childNodeRef == null) {
                        ChildAssociationRef childAssociationRef = nodeService
                                .createNode(parentNodeRef, containsAssocType, assocName, nodeType, typePropertiesMap); //mapCreator(typeProperties, namespaceService)
                        childNodeRef = childAssociationRef.getChildRef();
                        System.out.println(childNodeRef.toString() + "<<<<<------");
                    } else {
                        System.out.println(childNodeRef.toString() + "<<<<<------NULL");
                        nodeService.setType(childNodeRef, nodeType);
                        nodeService.setProperties(childNodeRef, typePropertiesMap);//mapCreator(typeProperties, namespaceService)
                    }

                    for (int i = 0; i < aspectsArray.length(); i++) {
                        JSONObject aspectObject = aspectsArray.getJSONObject(i);

                        QName aspectName = QName.createQName(aspectObject.getString("name"), namespaceService);

                        JSONArray aspectProperties = aspectObject.getJSONArray("properties");

                        nodeService.addAspect(childNodeRef, aspectName, mapCreator(aspectProperties, namespaceService));
                    }
                    webScriptResponse.setStatus(Status.STATUS_CREATED);
                } else {
                    logger.error(ExceptionConstants.EXCEPTION_PARENT_NODE_NOT_FOUND);
                    throw new WebScriptException(Status.STATUS_NOT_FOUND, ExceptionConstants.EXCEPTION_PARENT_NODE_NOT_FOUND);
                }

            } else {
                logger.error(ExceptionConstants.EXCEPTION_INVALID_NODE_REFERENCE_FORMAT);
                throw new WebScriptException(Status.STATUS_BAD_REQUEST, ExceptionConstants.EXCEPTION_INVALID_NODE_REFERENCE_FORMAT);
            }

        }catch (JSONException e){
            logger.error("JSON Exception", e);
        }
    }

    private Map<QName, Serializable> mapCreator(JSONArray aspectProperties, NamespaceService namespaceService){
        Map<QName, Serializable> aspectPropertiesMap = new HashMap<>();
        for (int j = 0; j < aspectProperties.length(); j++) {
            JSONObject property = null;
            try {
                property = aspectProperties.getJSONObject(j);
                QName propertyName = QName.createQName(property.getString("name"), namespaceService);
                Serializable propertyValue = (Serializable) property.get("value");
                aspectPropertiesMap.put(propertyName, propertyValue);
            } catch (JSONException e) {
                logger.error("JSON Exception", e);
            }
        }
        return aspectPropertiesMap;
    }
}
