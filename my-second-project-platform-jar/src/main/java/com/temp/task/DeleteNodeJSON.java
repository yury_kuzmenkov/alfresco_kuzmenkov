package com.temp.task;

import com.temp.exceptionRespons.ExceptionConstants;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;

public class DeleteNodeJSON extends AbstractWebScript {

    static final Logger logger = LogManager.getLogger(DeleteNodeJSON.class);

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        JSONObject jsonContent = (JSONObject) webScriptRequest.parseContent();
        if (jsonContent == null){
            logger.error(ExceptionConstants.EXCEPTION_EMPTY_CONTENT);
            throw new WebScriptException(Status.STATUS_BAD_REQUEST , ExceptionConstants.EXCEPTION_EMPTY_CONTENT);
        }
        String nodeRefStr = null;
        try {
            nodeRefStr = jsonContent.getString("nodeRef");
        } catch (JSONException e) {
            logger.error(ExceptionConstants.EXCEPTION_EMPTY_JSON_BODE, e);
        }

        if (NodeRef.isNodeRef(nodeRefStr)) {
            NodeRef nodeRef = new NodeRef(nodeRefStr);

            NodeService nodeService = serviceRegistry.getNodeService();

            if (nodeService.exists(nodeRef)) {
                nodeService.deleteNode(nodeRef);

                webScriptResponse.setStatus(Status.STATUS_ACCEPTED);
            } else {
                logger.error(ExceptionConstants.EXCEPTION_NODE_NOT_FOUND);
                throw new WebScriptException(Status.STATUS_NOT_FOUND, ExceptionConstants.EXCEPTION_NODE_NOT_FOUND);
            }
        } else {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, ExceptionConstants.EXCEPTION_INVALID_NODE_REFERENCE_FORMAT);
        }
    }
}
