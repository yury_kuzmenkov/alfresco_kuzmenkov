package com.temp.task;

import com.temp.exceptionRespons.ExceptionConstants;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

public class GetNodeJSON extends AbstractWebScript {

    static final Logger logger = LogManager.getLogger(GetNodeJSON.class);

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {

            String nodeRefString = webScriptRequest.getParameter("childNode");

        if (NodeRef.isNodeRef(nodeRefString)) {

            NodeRef nodeRef = new NodeRef(nodeRefString);
            NodeService nodeService = serviceRegistry.getNodeService();
            NamespaceService namespaceService = serviceRegistry.getNamespaceService();

            if (nodeService.exists(nodeRef)) {
                JSONObject jsonResponse = new JSONObject();
                try {

                    String[] pathToNodeName = nodeService.getPath(nodeRef).toPrefixString(namespaceService).split("/");
                    jsonResponse.put("nodeRef", nodeRefString);
                    jsonResponse.put("nodeName", pathToNodeName[pathToNodeName.length - 1]);

                    String type = nodeService.getType(nodeRef).toPrefixString(namespaceService);
                    jsonResponse.put("type", type);
                    JSONArray aspectsArray = new JSONArray();
                    for (QName aspect: nodeService.getAspects(nodeRef)) {
                        aspectsArray.put(aspect.toPrefixString(namespaceService));
                    }
                    jsonResponse.put("aspects", aspectsArray);

                    Map<QName, Serializable> nodeProperties = nodeService.getProperties(nodeRef);
                    JSONArray propertiesArray = new JSONArray();
                    for (Map.Entry<QName, Serializable> propertyEntry: nodeProperties.entrySet()) {
                        JSONObject propertyObject = new JSONObject();
                        propertyObject.put("name", propertyEntry.getKey().toPrefixString(namespaceService));
                        propertyObject.put("value", propertyEntry.getValue());

                        propertiesArray.put(propertyObject);
                    }
                    jsonResponse.put("properties", propertiesArray);

                } catch (JSONException e) {
                    logger.error(ExceptionConstants.EXCEPTION_EMPTY_JSON_BODE, e);
                }

                webScriptResponse.setStatus(Status.STATUS_OK);
                webScriptResponse.setContentType("application/json");
                webScriptResponse.setContentEncoding("utf-8");
                webScriptResponse.getWriter().write(jsonResponse.toString());
            } else {
                throw new WebScriptException(Status.STATUS_NOT_FOUND, ExceptionConstants.EXCEPTION_NODE_NOT_FOUND);
            }
        } else {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "Invalid node reference parameter");
        }
    }
}
