package com.temp.exceptionRespons;

public interface ExceptionConstants {
    String EXCEPTION_EMPTY_CONTENT = "Empty content";
    String EXCEPTION_EMPTY_JSON_BODE = "Exception empty json bode";
    String EXCEPTION_NODE_NOT_FOUND = "Node not found";
    String EXCEPTION_INVALID_NODE_REFERENCE_FORMAT = "Invalid node reference format";
    String EXCEPTION_PARENT_NODE_NOT_FOUND = "Parent node not found";
}
